const router = require('express').Router()
const taskRouter = require('./v1/taskRouter.js')

router.use('/task', taskRouter)

module.exports = router;
